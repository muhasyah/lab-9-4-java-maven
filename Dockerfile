# Gunakan image Maven untuk build proyek
FROM maven:3.8.1-openjdk-8 AS build

# Set direktori kerja di dalam container
WORKDIR /app

# Salin file pom.xml dan src ke dalam direktori kerja
COPY pom.xml .
COPY src ./src

# Jalankan build Maven
RUN mvn clean package

# Gunakan image OpenJDK untuk menjalankan aplikasi
FROM openjdk:8-jre-alpine

# Set direktori kerja di dalam container
WORKDIR /app

# Salin jar file dari tahap build
COPY --from=build /app/target/my-maven-project-1.0-SNAPSHOT.jar app.jar

# Tentukan perintah untuk menjalankan aplikasi
ENTRYPOINT ["java", "-jar", "app.jar"]
